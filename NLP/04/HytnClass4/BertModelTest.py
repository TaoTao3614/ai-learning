from transformers import BertModel, BertTokenizer
import torch
import torch.nn as nn

sentence = 'i like eating apples very much'


class Model(nn.Module):
    def __init__(self):
        super().__init__()
        self.embedder = BertModel.from_pretrained('bert-base-cased', output_hidden_states=True)
        self.tokenizer = BertTokenizer.from_pretrained('bert-base-cased')

    def forward(self, inputs):
        tokens = self.tokenizer.tokenize(inputs)
        print(tokens)
        tokens_id = self.tokenizer.convert_tokens_to_ids(tokens)
        print(tokens_id)
        tokens_id_tensor = torch.tensor(tokens_id).unsqueeze(0)
        outputs = self.embedder(tokens_id_tensor)
        print(outputs[0])


model = Model()
results = model(sentence)








